# Requirements

1. PHP 7+
1. MySQL database connection details

# Create database table

Create a table in the database to store/save our guestbook entries

```mysql
create table guestbook (
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    message text,
    ip_address varchar(255),
    created_at datetime
);
```

# Step 1

Create file `guestbook.php` with the following content:

```php
<html>
<head>
    <title>My guestbook</title>
</head>
<body>
<?php
    // Code section 1

    // Enter the database connection details below
    $db_host = ''; 
    $db_port = '';
    $db_user = ''; 
    $db_pwd = ''; 
    $db_name = ''; 

    $mysqli = mysqli_init() or die("Can not init mysqli");
    $mysqli->real_connect($db_host, $db_user, $db_pwd, $db_name, $db_port) or die("Failed to connect to the database");

    $today = date('D, d-M-Y');
?>
<h1>Welcome to my guestbook</h1>
<h2>Leave me a message today (<?=$today?>) by filling out the online form below</h2>
<form method="POST" action="">
    <fieldset>
        <label>First name:
            <input type="text" name="first_name">
        </label>
    </fieldset>
    <fieldset>
        <label>Last name:
            <input type="text" name="last_name">
        </label>
    </fieldset>
    <fieldset>
        <label>Email (will not be publicly displayed):
            <input type="text" name="email">
        </label>
    </fieldset>
    <fieldset>
        <label>Message:
            <textarea name="message"></textarea>
        </label>
    </fieldset>
    <p>
        <input type="submit" value="Submit">
    </p>
</form>
<h2>Guestbook Entries</h2>
<?php
    // Code section 2
    echo "<p>There are no entries yet, be the first!</p>";
?>
</body>
</html>
<?php 
    $mysqli->close(); // close the database connection opened on top
?>
```

Try load it on the browser, you should see the form.

# Step 2

Replace the `Code section 2` below

```php
    // Code section 2
    echo "<p>There are no entries yet, be the first!</p>";
```

With:

```php
    // Code section 2
    
    if (!empty($_POST['submit_button']) && $_POST['submit_button'] === 'Submit') {
        $debug = add_new_guestbook_entry($mysqli);
        echo "<pre>".print_r($debug, true)."</pre>";
    }

    display_guestbook_entries($mysqli);

    function add_new_guestbook_entry(mysqli $db) : array
    {
        $debug = [];

        // this is how to get the user IP address
        $ip_address = $_SERVER['REMOTE_ADDR'];

        // this is insecure and bad!!!!
        // but we will use this for learning about security later!!!!
        $sql_query = sprintf("INSERT INTO guestbook (first_name, last_name, email, message, ip_address, created_At) 
            VALUES ('%s', '%s', '%s', '%s', '%s', NOW())", 
            $_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['message'], $ip_address
        );

        $debug[] = $sql_query;

        $result = $db->query($sql_query);

        if (!$result) {
            $debug[] = mysqli_error($db);
        }

        return $debug;
    }

    function display_guestbook_entries(mysqli $db)
    {
        /** @var mysqli_result $result */
        $result = $db->query("SELECT * FROM guestbook ORDER BY created_at DESC LIMIT 1000");
        if ($result->num_rows > 0) {
            while ($entry = $result->fetch_assoc()) {
                // this is insecure and bad!!!!
                // but we will use this for learning about security later!!!!
                echo sprintf("<p>%s %s %s %s</p>", $entry['first_name'], $entry['last_name'], $entry['created_at'], $entry['message']);
            }
        } else {
            echo "<p>There are no entries yet, be the first!</p>";
        }
    }
```


# Exercise: date function

Modify the date format on the line `$today = date('D, d-M-Y')` after reading the `PHP date function documentation` at https://www.php.net/manual/en/function.date

